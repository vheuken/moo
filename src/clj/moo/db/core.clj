(ns moo.db.core
  (:require [monger.core :as mg]
            [monger.collection :as mc]
            [monger.operators :refer :all]
            [mount.core :refer [defstate]]
            [moo.config :refer [env]]))

(defstate db*
  :start (-> env :database-url mg/connect-via-uri)
  :stop (-> db* :conn mg/disconnect))

(defstate db
  :start (:db db*))

(defn get-room-by-name [name]
  (dissoc (mc/find-one-as-map db "rooms" {:name name}) :_id))

(defn set-room-state [state]
  (mc/update db "rooms" {:name (:name state)} {$set state})
  state)

(defn create-room [name]
  (mc/insert db "rooms" {:name name
                         :users []
                         :tracks []
                         :syncs {}
                         :player {:paused? false
                                  :autoplay? true}}))

(defn add-user-to-room [room-name user-name user-id]
  (mc/update db "rooms" {:name room-name}
             {$push {:users {:name user-name
                             :id user-id}}}))

(defn remove-user-from-room [room-name user-id]
  (mc/update db "rooms" {:name room-name}
             {$pull {:users {:id user-id}}}))

(defn delete-track [room-name track-id]
  (mc/update db "rooms" {:name room-name}
             {$pull {:tracks {:id track-id}}}))

(defn delete-room [room-name]
  (mc/remove db "rooms" {:name room-name}))

(defn add-track-to-room [room-name track]
  (mc/update db "rooms" {:name room-name}
             {$push {:tracks track}}))

(defn update-track [room-name track]
  (mc/update db "rooms" {:name room-name :tracks.id (:id track)}
             {$set {"tracks.$" track}}
             {:multi false}))

(defn set-track [room-name track-id pos]
  (mc/update db "rooms" {:name room-name}
             {$set {:player.track-id track-id
                    :player.player-pos (assoc pos :last-start-timestamp
                                              (quot (System/currentTimeMillis) 1000))}}))

(defn pause [room-name pos]
  (mc/update db "rooms" {:name room-name}
             {$set {:player.paused? true
                    :player.player-pos.last-stop-point pos}}))

(defn unpause [room-name]
  (mc/update db "rooms" {:name room-name}
             {$set {:player.paused? false
                    :player.player-pos.last-start-timestamp (quot (System/currentTimeMillis) 1000)}}))

(defn enable-autoplay [room-name]
  (mc/update db "rooms" {:name room-name}
             {$set {:player.autoplay? true}}))

(defn disable-autoplay [room-name]
  (mc/update db "rooms" {:name room-name}
             {$set {:player.autoplay? false}}))

(defn set-track-at-index [room-name track index]
  (mc/update db "rooms" {:name room-name}
             {$push {:tracks {$each [track]
                              "$position" index}}}))

(defn get-all-in-progress-uploads-and-downloads []
  (mc/aggregate  db "rooms" [{$unwind "$tracks"}
                             {$match {$or [{:tracks.track-type "download"}
                                           {:tracks.track-type "upload"}]}}
                             {$project {:track-id "$tracks.id"
                                        :url "$tracks.url"
                                        :room-id "$name"}}]
                 :cursor {:batch-size 20}))
