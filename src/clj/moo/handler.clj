(ns moo.handler
  (:require [compojure.core :refer [routes wrap-routes defroutes GET POST]]
            [ring.middleware.partial-content :refer [wrap-partial-content]]
            [ring.middleware.resource :refer [wrap-resource]]
            [immutant.web.middleware :refer [wrap-session]]
            [raven-clj.ring :refer [wrap-sentry]]
            [clojure.tools.logging :as log]
            [moo.layout :as layout :refer [error-page]]
            [moo.routes.home :refer [home-routes]]
            [compojure.route :as route]
            [moo.env :refer [defaults]]
            [mount.core :as mount]
            [moo.middleware :as middleware]
            [moo.ws :as ws]
            [moo.utils :as utils]
            [moo.actions :as actions]
            [moo.handlers.core :as handlers]
            [moo.handlers.chat]
            [moo.handlers.player]
            [moo.handlers.upload]
            [moo.download :as download]
            [moo.upload :as upload]
            [moo.db.core :as db]
            [moo.cache-manager.core :as cache]
            [moo.cache-manager.actions]
            [moo.room.events]
            [moo.room.actions]
            [moo.track.events]
            [moo.track.actions]
            [moo.config :refer [env]]
            [moo.error :as error]))

(mount/defstate init-app
  :start ((or (:init defaults) identity))
  :stop  ((or (:stop defaults) identity)))

(def database-fns {:get-room-state-fn db/get-room-by-name})

(defn event-msg-handler [{{room-id :room-id
                           cookies :cookies
                           :as ring-req} :ring-req
                          uid :uid
                          :as event-msg}]
  (future
    (log/info (:event event-msg))
    (locking room-id
      (try
        (let [session-id (:value (get cookies "JSESSIONID"))
              user (if-let [user (cache/fetch :users session-id)]
                     user
                     {:id (utils/uuid) :session-id session-id})
              clients-in-room (cache/fetch :room-clients room-id)
              client (get clients-in-room uid)
              actions (handlers/-event-msg-handler (assoc event-msg
                                                          :room-id room-id
                                                          :user user
                                                          :session-id session-id
                                                          :client client
                                                          :clients-in-room clients-in-room
                                                          :downloads-dir download/downloads-dir
                                                          :uploads-dir upload/uploads-dir
                                                          :unique-id-fn utils/uuid
                                                          :database-fns database-fns
                                                          :config env))]
          (log/info "Actions:" actions)
          (actions/execute-server-actions! actions))
        (catch Exception e
          (error/report-error e (:event event-msg)))))))

(defn add-user [req]
  (if-let [session-id (:value ((:cookies req) "JSESSIONID"))]
    (assoc-in req [:session :user] (cache/fetch :users session-id))
    req))

(defroutes room-routes
  (GET "/room/:room-id" req (assoc-in (layout/render "room.html" {:sentry-dsn (:sentry-dsn env)
                                                                  :upload-file-size-limit (:upload-file-size-limit env)})
                                      [:cookies "user-id" :value]
                                      (if-let [user (:user (:session (add-user req)))]
                                        (:id user)
                                        "")))
  (GET "/room/:room-id/chsk" req (ws/ring-ajax-get-or-ws-handshake req))
  (POST "/room/:room-id/upload" req  (upload/upload (add-user req))))

(def app-routes
  (routes
   (-> room-routes
       (wrap-sentry (:sentry-dsn env))
       (wrap-routes middleware/wrap-csrf)
       (wrap-routes middleware/wrap-formats)
       ring.middleware.keyword-params/wrap-keyword-params
       ring.middleware.params/wrap-params)
   (wrap-partial-content (route/files "/static" {:root (str (System/getProperty "user.dir") "/static/")}))
   (route/not-found
    (:body
     (error-page {:status 404
                  :title "page not found"})))))

(defn app [] (middleware/wrap-base #'app-routes))
