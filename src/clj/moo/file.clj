(ns moo.file
  (:require [pantomime.extract :as extract]
            [clojure.set :refer [rename-keys]]
            [clojure.algo.generic.functor :refer (fmap)]))

(defn get-metadata! [file]
  (fmap
    first
    (-> (extract/parse file)
        (select-keys [:xmpdm/tracknumber :author :title :album])
        (rename-keys {:xmpdm/tracknumber :track-number
                      :author :artist}))))
