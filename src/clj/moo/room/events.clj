(ns moo.room.events
  (:require [moo.handlers.core :as handler]
            [moo.db.core :as db]
            [clojure.data :refer [diff]]
            [clojure.string :as string]))

(defmethod handler/-event-msg-handler :room/user-joined [event-msg]
  (let [uid (:uid event-msg)
        user-name (:name (:?data event-msg))
        room-name (:room-id event-msg)
        room (db/get-room-by-name room-name)
        user (:user event-msg)
        user-id (:id user)]
    (when-not room
      (db/create-room room-name))
    (db/add-user-to-room room-name user-name user-id)
    [[:cache/put [:users (:session-id user) user]]
     [:room/broadcast [(:room-id event-msg) [:user/new {:name user-name
                                                        :id user-id}]]]]))

(defmethod handler/-event-msg-handler :chsk/uidport-close [{client :client :as event-msg}]
  (when client
    (handler/-event-msg-handler (assoc event-msg :id :client/disconnect))))

(defmethod handler/-event-msg-handler :chsk/uidport-open [event-msg]
  (handler/-event-msg-handler (assoc event-msg :id :client/connect)))

(defmethod handler/-event-msg-handler :client/connect [{room-id :room-id
                                                        clients-in-room :clients-in-room
                                                        uid :uid
                                                        session-id :session-id
                                                        user :user
                                                        {get-room-state :get-room-state-fn} :database-fns}]
  [[:cache/put [:room-clients room-id (assoc clients-in-room uid {:id uid :synced? true})]]
   [:cache/put [:users
                (:session-id user)
                (if (:clients user)
                  (update-in user [:clients] conj uid)
                  (assoc user :clients #{uid}))]]
   [:send-to-client [uid [:room/state-update (get-room-state room-id)]]]])

(defn all-users-clients-disconnected [room-id clients-in-room client user]
  [[:cache/remove [:room-clients room-id]]
   [:cache/remove [:users (:session-id user)]]
   (if (= 0 (count (dissoc clients-in-room (:id client))))
     [:room/delete room-id]
     [:room/remove-user [room-id (:id user)]])])

(defmethod handler/-event-msg-handler :client/disconnect [{room-id :room-id
                                                           user :user
                                                           clients-in-room :clients-in-room
                                                           client :client
                                                           :as event-msg}]
  (conj
    (if (empty? (disj (:clients user) (:id client)))
      (all-users-clients-disconnected room-id clients-in-room client user)
      [[:cache/put [:room-clients room-id (dissoc clients-in-room (:id client))]]
       [:cache/put [:users
                    (:session-id user)
                    (update-in user [:clients] disj (:id client))]]])
    (when (and (:synced? client)
               (not (:ended? client)))
      (first (handler/-event-msg-handler (assoc event-msg :id :player/ended))))))
