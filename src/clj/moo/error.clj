(ns moo.error
  (:require [raven-clj.core :refer [capture]]
            [raven-clj.interfaces :as interfaces]
            [clojure.tools.logging :as log]
            [moo.config :refer [env]]))

(defn report-error [e extra]
  (log/error "Exception thrown: " e "\nDebug data:" extra)
  (capture (:sentry-dsn env)
           (-> {:message (str "Exception thrown: " (.getMessage e))
                :extra extra}
               (interfaces/stacktrace e)))
  (throw e))
