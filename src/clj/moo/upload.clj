(ns moo.upload
  (:require [clojure.string :as string]
            [clojure.tools.logging :as log]
            [moo.utils :as utils]
            [moo.handlers.core :as handlers]
            [moo.actions :as actions]
            [moo.file :as file]
            [clojure.java.io :as io]
            [mount.core :as mount]
            [immutant.caching :as c]))

(def uploads-dir (str (System/getProperty "user.dir") "/static/"))

(defn move-file! [source-file target-file]
  (java.nio.file.Files/move source-file target-file
                            (into-array java.nio.file.CopyOption [(java.nio.file.StandardCopyOption/ATOMIC_MOVE)
                                                                  (java.nio.file.StandardCopyOption/REPLACE_EXISTING)])))

(defn upload [req]
  (log/error req)
  (let [original-file (:tempfile (:upload-file (:params req)))
        filename (:filename (:upload-file (:params req)))
        file-extension (last (string/split filename #"\."))
        track-id (:track-id (:params req))
        new-file (io/file uploads-dir (str track-id "." file-extension))]
    (.renameTo original-file new-file)
    (future
      (locking (:room-id (:params req))
        (let [room-id (:room-id (:params req))
              user (:user (:session req))
              track {:id track-id
                     :track-type :file
                     :metadata (file/get-metadata! new-file)
                     :src (str "/static/" (str track-id "." file-extension))}]
          (actions/execute-server-actions! [[:update-track [room-id track]]
                                            [:room/broadcast [room-id [:track/update track]]]])))))
  "OK")

(defn file-store! [{:keys [filename content-type stream]}]
  (let [f (io/file (str uploads-dir (utils/uuid) ".tmp"))]
    (io/copy stream f)
    {:filename filename :content-type content-type :tempfile f :size (.length f)}))

(mount/defstate progress-interval-cache :start (c/cache "progress-interval-cache"
                                                        :idle [1 :minute]))
(defn progress-fn! [{params :params
                     room-id :room-id
                     :as request}
                    bytes-read content-length item-count]
  (let [track-id (get params "track-id")]
    (if-let [last-time (get progress-interval-cache track-id)]
      (let [current-time (System/currentTimeMillis)]
        (when (< 333 (- current-time last-time))
          (actions/execute-server-actions! [[:room/broadcast
                                             [room-id [:track/update {:id track-id
                                                                      :track-type :upload
                                                                      :bytes-read bytes-read
                                                                      :content-length content-length}]]]])
          (.put progress-interval-cache track-id current-time)))
      (.put progress-interval-cache track-id (System/currentTimeMillis)))))
