(ns moo.cache-manager.core
  (:require [immutant.caching :as c]
            [mount.core :as mount]))

(mount/defstate caches
                :start (atom {}))

(defn remove! [id k]
  (when-let [cache (get @caches id)]
    (.remove cache k)))

(defn fetch [id k]
  "Attempts to get a value from the cache associated with the id parameter
   If no cache is associated with the supplied id, nil is returned"
  (when-let [cache (@caches id)]
    (.get cache k)))

(defn put-or-create! [id k v & opts]
  (if-let [cache (get @caches id)]
    (.put cache k v)
    (let [args (cons (str id) opts)
          cache (apply c/cache args)]
      (swap! caches assoc id cache)
      (.put cache k v))))
