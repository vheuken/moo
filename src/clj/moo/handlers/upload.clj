(ns moo.handlers.upload
  (:require [moo.handlers.core :as handler]))

(defmethod handler/-event-msg-handler :upload/new [{client :client
                                                    room-id :room-id
                                                    user :user
                                                    client-upload-id :?data
                                                    unique-id-fn :unique-id-fn}]
  (let [track {:id (unique-id-fn)
               :user-id (:id user)
               :type "upload"}]
    [[:add-track [room-id track]]
     [:room/broadcast [room-id [:track/new track]]]
     [:send-to-client [(:id client) [:upload/start {:track-id (:id track)
                                                    :client-upload-id client-upload-id
                                                    :start-byte 0}]]]]))
