(ns moo.core
  (:require [moo.handler :as handler]
            [moo.ws :as ws]
            [luminus.repl-server :as repl]
            [luminus.http-server :as http]
            [moo.config :refer [env]]
            [clojure.tools.cli :refer [parse-opts]]
            [clojure.tools.logging :as log]
            [mount.core :as mount]
            [raven-clj.core :as raven]
            [moo.db.core :as db]
            [moo.actions :as actions])
  (:gen-class))

(def cli-options
  [["-p" "--port PORT" "Port number"
    :parse-fn #(Integer/parseInt %)]])

(mount/defstate ^{:on-reload :noop}
  http-server
  :start
  (http/start
   (-> env
       (assoc :host "127.0.0.1"
              :handler (handler/app))
       (update :io-threads #(or % (* 2 (.availableProcessors (Runtime/getRuntime)))))
       (update :port #(or (-> env :options :port) %))))
  :stop
  (http/stop http-server))

(mount/defstate ^{:on-reload :noop}
  repl-server
  :start
  (when-let [nrepl-port (env :nrepl-port)]
    (repl/start {:port nrepl-port}))
  :stop
  (when repl-server
    (repl/stop repl-server)))

(mount/defstate socket-server
  :start (ws/start-socket-server! handler/event-msg-handler)
  :stop ((:stop-fn socket-server)))

(defn resume-downloads! []
  (log/info "Resuming downloads!")
  (let [tracks (db/get-all-in-progress-uploads-and-downloads)]
    (log/info "Resuming " (count tracks) " downloads/uploads")
    (doseq [track tracks]
      (actions/-execute-server-action! [:download-track track]))))

(defn stop-app []
  (doseq [component (:stopped (mount/stop))]
    (log/info component "stopped"))
  (shutdown-agents))

(defn start-app [args]
  (doseq [component (-> args
                        (parse-opts cli-options)
                        mount/start-with-args
                        :started)]
    (log/info component "started"))
  (.addShutdownHook (Runtime/getRuntime) (Thread. stop-app))
  (resume-downloads!))

(defn -main [& args]
  (raven/install-uncaught-exception-handler! (:sentry-dsn env) {})
  (start-app args))
