(ns moo.shared.utils)

(defn get-next-track [tracks current-track-id]
  (let [ids (map :id tracks)
        current-track-index (.indexOf ids current-track-id)]
    (when (< current-track-index (- (count ids) 1))
      (nth tracks (+ 1 current-track-index)))))
