(ns moo.core
  (:require [reagent.core :as r]
            [re-frame.core :as rf]
            [secretary.core :as secretary]
            [goog.events :as events]
            [goog.history.EventType :as HistoryEventType]
            [goog.net.cookies]
            [markdown.core :refer [md->html]]
            [ajax.core :refer [GET POST]]
            [moo.ajax :refer [load-interceptors!]]
            [moo.ws :as ws]
            [moo.player :as player]
            [moo.chat :as chat]
            [moo.utils :as utils]
            [moo.events]
            [cljsjs.raven])
  (:import goog.History))

(defn file-upload []
  (let [file-target (r/atom nil)]
    (fn []
      [:div
       [:input {:name "file-input" :type "file" :on-change #(reset! file-target (.-target %))}]
       [:button {:on-click #(rf/dispatch [:upload/new @file-target])} "Upload a File"]])))

(defn new-url-track-input []
  (let [text (r/atom "")]
    (fn []
      [:div
       [:input {:type "text"
                :placeholder "Enter MP3 or MP4 URL"
                :value @text
                :on-change #(reset! text (-> % .-target .-value))}]
       [:button {:on-click #(rf/dispatch [:track/add-external @text])}
        "New MP4 track"]])))

(defn new-yt-track-input []
  (let [text (r/atom "")]
    (fn []
      [:div
       [:input {:type "text"
                :placeholder "Enter Youtube url"
                :value @text
                :on-change #(reset! text (-> % .-target .-value))}]
       [:button {:on-click #(rf/dispatch [:track/add-youtube @text])}
        "New Youtube track"]])))

(defn media-inputs []
  [:div.row {:style {:height "15%"}}
   [new-yt-track-input]
   [new-url-track-input]
   [file-upload]])

(defn track-list []
  [:div.row.h-25.w-100 {:style {:overflowY "scroll"}}
   (let [users @(rf/subscribe [:users])]
     [:ol (map (fn [{:keys [id user-id metadata bytes-read content-length track-type]}]
                 [:li {:key id}
                  [:div
                   (when (= :download track-type)
                     (str "Downloading to server: " (* 100 (/ bytes-read content-length)) "%"))
                   (when (= :upload track-type)
                     (str "Uploading to server: " (* 100 (/ bytes-read content-length)) "%"))
                   (:artist metadata) " - "
                   (:title metadata) " - "
                   (:name (first (filter #(= (:id %) user-id) users)))
                   [:button {:on-click #(rf/dispatch [:player/set-track {:track-id id
                                                                         :player-pos {:last-stop-point 0
                                                                                      :last-start-timestamp (utils/get-unix-timestamp)}}])}
                    "Select"]
                   [:button {:on-click #(rf/dispatch [:track/remove id])} "Remove"]
                   [:button {:on-click #(rf/dispatch [:track/move-down id])} "▲"]
                   [:button {:on-click #(rf/dispatch [:track/move-up id])} "▼"]]])
               @(rf/subscribe [:tracks]))])])

(defn room-page []
  [:div.container-fluid {:style {:height "100%"}}
   [:div.row {:style {:height "100%"}}
    [:div.col-4 {:style {:padding 0 :height "100%"}} [chat/chat]]
    [:div.col-8 {:style {:height "100%"}}
     [player/player-component]
     [media-inputs]
     [track-list]]]])

(defn login-page []
  (let [text (r/atom "")]
    (fn []
      [:div
       [:div.col
        {:style #js {:position "absolute" :zIndex 1 :width "100%" :height "100%" :backgroundColor "rgba(0, 0, 0, 0.5)"}}
        [:div.row.justify-content-center "Enter username: "]
        [:div.row.justify-content-center
         [:input {:type "text"
                  :placeholder "Username"
                  :value @text
                  :on-change #(reset! text (-> % .-target .-value))}]
         [:button {:on-click #(when-not (empty? @text)
                                (rf/dispatch [:join-room {:name @text}]))}
          "Join"]]]
       [room-page]])))

(def pages
  {:login #'login-page
   :room #'room-page})

(defn page []
  [:div
   [(pages @(rf/subscribe [:page]))]])

;; -------------------------
;; Routes
;; (secretary/set-config! :prefix "#")

;(secretary/defroute "/" []
;  (rf/dispatch [:set-active-page :home]))

;(secretary/defroute "/about" []
;  (rf/dispatch [:set-active-page :about]))

;; -------------------------
;; History
;; must be called after routes have been defined
(defn hook-browser-navigation! []
  (doto (History.)
    (events/listen
     HistoryEventType/NAVIGATE
     (fn [event]
       (secretary/dispatch! (.-token event))))
    (.setEnabled true)))

;; -------------------------
;; Initialize app
(defn mount-components []
  (rf/clear-subscription-cache!)
  (r/render [#'page] (.getElementById js/document "app")))

(defn add-exit-listener! []
  (.addEventListener js/window "beforeunload" #(rf/dispatch [:disconnect])))

(defn init! []
  (letfn [(init []
            (rf/dispatch-sync [:initialize-db])
            (rf/dispatch-sync [:set-user-id (.get goog.net.cookies "user-id")])
            (add-exit-listener!)
            (hook-browser-navigation!)
            (mount-components)
            (load-interceptors!)
            (ws/start-router!)
            (player/start-position-bar-watcher!))]
    (if (and (exists? js/Raven)
             (not (empty? js/sentryDSN)))
      (do
        (.install (.config js/Raven js/sentryDSN))
        (.context js/Raven #(init)))
      (init))))
