(ns moo.ws
  (:require-macros
   [cljs.core.async.macros :as asyncm :refer (go go-loop)])
  (:require
   [cljs.core.async :as async :refer (<! >! put! chan)]
   [taoensso.sente  :as sente :refer (cb-success?)]
   [re-frame.core :as rf]
   [moo.utils :as utils]))

(defn init-ws-handler! []
  (let [{:keys [chsk ch-recv send-fn state]}
        (sente/make-channel-socket! (str "/room/" (utils/get-room-id) "/chsk") {:type :ws})]
    (defonce ch-chsk ch-recv) ; ChannelSocket's receive channel
    (defonce send! send-fn))) ; ChannelSocket's send API fn

(defmulti event-handler :id)

(defmethod event-handler :chsk/recv [event-msg]
  (println "Received event to pass to handler: " event-msg)
  (rf/dispatch [(first (:?data event-msg))
                (second (:?data event-msg))
                {:from-server? true}]))

(defmethod event-handler :default [event-msg]
  (println "Unhandled event" event-msg))

(defn start-router! []
  (init-ws-handler!)
  (sente/start-client-chsk-router! ch-chsk event-handler))
