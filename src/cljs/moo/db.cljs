(ns moo.db
  (:require [cljs.spec.alpha :as s]))

(s/def ::pending-uploads map?)
(s/def ::users vector?)
(s/def ::messages vector?)
(s/def ::tracks vector?)

(s/def ::syncs map?)

(s/def ::autoplay? boolean?)
(s/def ::paused? boolean?)
(s/def ::last-stop-point number?)
(s/def ::last-start-timestamp int?)
(s/def ::track-id (s/nilable string?))
(s/def ::player-pos (s/keys :req-un [::last-stop-point ::last-start-timestamp]))
(s/def ::player (s/keys :req-un [::paused? ::autoplay?]
                        :opt-un [::player-pos ::track-id]))

(s/def ::room-state (s/keys :req-un [::syncs ::users ::tracks ::player]))
(s/def ::db (s/keys :req-un [::room-state ::messages ::pending-uploads]))

(def default-db
  {:page :login
   :volume 1
   :synced? true
   :upload-file-size-limit (int js/uploadFileSizeLimit)
   :messages []
   :pending-uploads {}
   :room-state {:users []
                :tracks []
                :syncs {}
                :player {:paused? true
                         :autoplay? true}}})
