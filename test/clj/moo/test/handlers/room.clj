(ns moo.test.handlers.room
  (:require [moo.handlers.core :refer :all]
            [moo.handlers.room]
            [clojure.test :refer :all]))

(deftest client-connect
  (testing "Should create a new, synced client and create new user when no user exists "
    (let [room-id "test-room-id"
          uid "test-uid"
          room-state {:r room-id}
          session-id "test-session-id"
          user-id "test-user-id"
          actions (-event-msg-handler {:id :client/connect
                                       :room-id room-id
                                       :uid uid
                                       :user {:id user-id :session-id session-id}
                                       :session-id session-id
                                       :database-fns {:get-room-state-fn (fn [id] {:r id})}})]
      (is (= [:update-client [room-id {:id uid :synced? true}]] (nth actions 0)))
      (is (= [:update-user {:id user-id :session-id session-id :clients #{uid}}] (nth actions 1)))
      (is (= [:send-to-client [uid [:room/state-update room-state]]] (nth actions 2)))))

  (testing "Should create a new, synced client and add new client id to user"
    (let [room-id "test-room-id"
          user-id "test-user-id"
          old-uid "old-test-uid"
          new-uid "new-test-uid"
          room-state {:r room-id}
          session-id "test-session-id"
          user-id "test-user-id"
          actions (-event-msg-handler {:id :client/connect
                                       :room-id room-id
                                       :uid new-uid
                                       :session-id session-id
                                       :user {:id user-id :clients #{old-uid} :session-id session-id}
                                       :database-fns {:get-room-state-fn (fn [id] {:r id})}})]
      (is (= [:update-client [room-id {:id new-uid :synced? true}]] (nth actions 0)))
      (is (= [:update-user {:id user-id :session-id session-id :clients #{new-uid old-uid}}] (nth actions 1)))
      (is (= [:send-to-client [new-uid [:room/state-update room-state]]] (nth actions 2))))))

(deftest client-disconnect
  (testing "Should delete the client and remove it from user"
    (let [client-id "test-client-id"
          other-client-id "test-other-client-id"
          room-id "test-room-id"
          user-id "test-user-id"
          session-id "test-session-id"
          actions (-event-msg-handler {:id :client/disconnect
                                       :client {:id client-id}
                                       :user {:id user-id :clients #{client-id other-client-id} :session-id session-id}
                                       :room-id room-id})]
      (is (= [:delete-client [room-id client-id]] (nth actions 0)))
      (is (= [:update-user {:id user-id :clients #{other-client-id} :session-id session-id}] (nth actions 1)))))

  (testing "Should delete the client and delete user if it was the user's last client"
    (let [client-id "test-client-id"
          room-id "test-room-id"
          user-id "test-user-id"
          session-id "test-session-id"
          actions (-event-msg-handler {:id :client/disconnect
                                       :client {:id client-id}
                                       :user {:id user-id :clients #{client-id} :session-id session-id}
                                       :room-id room-id})]
      (is (= [:delete-client [room-id client-id]] (nth actions 0)))
      (is (= [:cache/remove [:users session-id]] (nth actions 1)))
      (is (= [:delete-room room-id] (nth actions 2)))))

  (testing "Should delete room if there are no other clients"
    (let [client-id "test-client-id"
          room-id "test-room-id"
          user-id "test-user-id"
          session-id "test-session-id"
          actions (-event-msg-handler {:id :client/disconnect
                                       :client {:id client-id}
                                       :user {:id user-id :clients #{client-id} :session-id session-id}
                                       :clients-in-room {client-id {:id client-id}}
                                       :room-id room-id})]
      (is (= [:delete-client [room-id client-id]] (nth actions 0)))
      (is (= [:cache/remove [:users session-id]] (nth actions 1)))
      (is (= [:delete-room room-id] (nth actions 2)))))

  (testing "Should send player/ended signal if synced and not ended"
    (let [client-id "test-client-id"
          client {:id client-id :synced? true}
          room-id "test-room-id"
          user-id "test-user-id"
          session-id "test-session-id"
          event-msg {:id :client/disconnect
                     :client client
                     :user {:id user-id :clients #{client-id} :session-id session-id}
                     :clients-in-room {client-id {:id client-id}
                                       "test" {:id "test"}}
                     :room-id room-id
                     :database-fns {:get-room-state-fn
                                    (fn [id]
                                      (get {room-id {:player {:r room-id}}} id))}}
          actions (-event-msg-handler event-msg)]
      (is (= (first (-event-msg-handler (assoc event-msg :id :player/ended))) (nth actions 3))))))
