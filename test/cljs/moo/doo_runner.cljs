(ns moo.doo-runner
  (:require [doo.runner :refer-macros [doo-tests]]
            [moo.core-test]
            [moo.events-test]))

(doo-tests 'moo.core-test
           'moo.events-test)
