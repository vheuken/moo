(ns moo.events-test
  (:require [cljs.test :refer-macros [is are deftest testing use-fixtures]]
            [pjstadig.humane-test-output]
            [reagent.core :as reagent :refer [atom]]
            [moo.events :as events]))

(deftest syncable-synced-from-server
  (let [cofx {:db {:synced? true}}
        event [:test-id {:test-data 1} {:from-server? true}]
        apply-locally (fn [cofx event-data] {:cofx cofx :event-data event-data})]
    (is (= {:cofx cofx :event-data (second event)}
           (events/syncable cofx event apply-locally)))))

(deftest syncable-desynced-from-server
  (let [cofx {:db {:synced? false}}
        event [:test-id {:test-data 1} {:from-server? true}]
        apply-locally (fn [cofx event-data] {:cofx cofx :event-data event-data})]
    (is (= {}
           (events/syncable cofx event apply-locally)))))

(deftest syncable-synced-from-client
  (let [cofx {:db {:synced? true}}
        event [:test-id {:test-data 1} {:from-server? false}]
        apply-locally (fn [cofx event-data] {:cofx cofx :event-data event-data})
        to-server (fn [event-data] event-data)]
    (is (= {:cofx cofx :event-data (second event) :ws-send [:test-id (second event)]}
           (events/syncable cofx event apply-locally to-server)))))

(deftest syncable-desynced-from-client
  (let [cofx {:db {:synced? false}}
        event [:test-id {:test-data 1} {:from-server? false}]
        apply-locally (fn [cofx event-data] {:cofx cofx :event-data event-data})]
    (is (=  {:cofx cofx :event-data (second event)}
            (events/syncable cofx event apply-locally)))))
